import mechanize
import cookielib
from bs4 import BeautifulSoup
import xlrd
import xlsxwriter

try:
    # Read the input file
    book = xlrd.open_workbook("input.xlsx")

    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook("results.xlsx")
    worksheet = workbook.add_worksheet()
    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})
    # Text with formatting.
    worksheet.write('A1', 'App No', bold)
    worksheet.set_column('A:A', 10)
    worksheet.write('B1', 'DOB', bold)
    worksheet.set_column('B:B', 10)
    worksheet.write('C1', 'Roll', bold)
    worksheet.set_column('C:C', 10)
    worksheet.write('D1', 'Name', bold)
    worksheet.set_column('D:D', 25)
    worksheet.write('E1', 'Fathers Name', bold)
    worksheet.set_column('E:E', 22)
    worksheet.write('F1', 'Physics', bold)
    worksheet.write('G1', 'Chemistry', bold)
    worksheet.set_column('G:G', 10)
    worksheet.write('H1', 'Math', bold)
    worksheet.write('I1', 'Total', bold)
    worksheet.write('J1', 'Qualified?', bold)
    worksheet.set_column('J:J', 10)
    worksheet.write('K1', 'Category', bold)

    # Get the sheet from input file
    sheet = book.sheet_by_index(0)
    rowCount = sheet.nrows
    for index in xrange(1, rowCount):
        # Reading student info
        appNo = int(sheet.cell(rowx=index, colx=0).value)
        print 'Processing', appNo
        regNo = int(sheet.cell(rowx=index, colx=2).value)
        year, month, date, hour, minute, second = xlrd.xldate_as_tuple(sheet.cell_value(rowx=index, colx=1),
                                                                       book.datemode)
        date = '0' + str(date) if date < 10 else str(date)
        month = '0' + str(month) if month < 10 else str(month)
        dob = str(date) + '/' + str(month) + '/' + str(year)
        worksheet.write('A' + str(index + 1), appNo)
        # print index + 1, appNo, regNo, dob

        # Getting Student Info
        # Browser
        br = mechanize.Browser()

        # Cookie Jar
        cj = cookielib.LWPCookieJar()
        br.set_cookiejar(cj)

        # Browser options
        br.set_handle_equiv(True)
        br.set_handle_gzip(True)
        br.set_handle_redirect(True)
        br.set_handle_referer(True)
        br.set_handle_robots(False)

        # Follows refresh 0 but not hangs on refresh > 0
        br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

        # User-Agent (this is cheating, ok?)
        br.addheaders = [('User-agent',
                          'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

        # The site we will navigate into, handling it's session
        br.open('http://cbseresults.nic.in/jee/jee_2016.htm')

        # Select the first (index zero) form
        br.select_form(nr=0)

        # Credentials
        br.form['regno'] = str(regNo)
        br.form['dob'] = str(dob)

        # Get result
        response = br.submit()

        # Reading result
        html = response.read()
        # html = open('input.html', 'r').read()
        soup = BeautifulSoup(html, "html.parser")

        # Student details
        tables = soup.find_all('table')
        details = tables[6].find_all('tr')

        roll = details[0].find_all('td')[1].text.strip()
        # print roll
        worksheet.write('C' + str(index + 1), roll)

        dob = details[1].find_all('td')[1].text.strip()
        # print dob
        worksheet.write('B' + str(index + 1), dob)

        name = details[2].find_all('td')[1].text
        worksheet.write('D' + str(index + 1), name)

        # mothersName = details[3].find_all('td')[1].text
        fathersName = details[4].find_all('td')[1].text
        worksheet.write('E' + str(index + 1), fathersName)

        category = tables[7].find_all('font')[0].text.split('\n')[0][9:].strip()
        worksheet.write('K' + str(index + 1), category)

        details = tables[8].find_all('tr')
        physics = int(details[1].find_all('td')[1].text.strip())
        worksheet.write('F' + str(index + 1), physics)

        chemistry = int(details[2].find_all('td')[1].text.strip())
        worksheet.write('G' + str(index + 1), chemistry)

        math = int(details[3].find_all('td')[1].text.strip())
        worksheet.write('H' + str(index + 1), math)

        total = int(details[4].find_all('td')[1].text.strip())
        worksheet.write('I' + str(index + 1), total)

        details = tables[13].find_all('tr')
        generalCutOff = int(details[1].find_all('td')[1].text.strip())
        obcCutOff = int(details[1].find_all('td')[1].text.strip())
        scCutOff = int(details[2].find_all('td')[1].text.strip())
        stCutOff = int(details[3].find_all('td')[1].text.strip())

        # Category Types: GENERAL, OBC-NCL, SC, ST
        qualified = False
        classificationError = False
        if category == 'GENERAL':
            qualified = True if total >= generalCutOff else False
        elif category == 'OBC-NCL':
            qualified = True if total >= obcCutOff else False
        elif category == 'SC':
            qualified = True if total >= scCutOff else False
        elif category == 'ST':
            qualified = True if total >= stCutOff else False
        else:
            classificationError = True

        if classificationError:
            worksheet.write('J' + str(index + 1), 'error')
        else:
            result = 'Yes' if qualified else 'No'
            worksheet.write('J' + str(index + 1), result)

    workbook.close()
except Exception:
    print 'Input file not found. Make sure input.xlsx exists'